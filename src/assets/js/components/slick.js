var photoSlide = $('#photo');

// Slick_slider_option
photoSlide.slick({
    dots: true,
    fade: true,
    cssEase: 'linear',
    arrows: false,
});

// Button_arrow_prev
$("#arrow_prev").on("click", function(event) {
    event.preventDefault();
    photoSlide.slick('slickPrev');
});

// Button_arrow_next
$("#arrow_next").on("click", function(event) {
    event.preventDefault();
    photoSlide.slick('slickNext');
});